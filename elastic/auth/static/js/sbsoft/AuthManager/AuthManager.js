define([
    "dojo/_base/declare",
    "dojo/on",
    "dojo/dom",
    "dojo/dom-style",
    "dojo/Deferred",
    "dojo/date/stamp",
    "dojo/cookie",
    "dojo/request/xhr",
    "dijit/registry",
    "sbsoft/AuthManager/RegistrationDialog",
    "sbsoft/AuthManager/VerifyDialog",
    "dijit/form/DropDownButton",
    "dijit/TooltipDialog",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_Container",
    "dojo/text!./resources/AuthManager.html"
], function(declare, on, dom, domStyle, Deferred, stamp, cookie, xhr, registry, RegistrationDialog, VerifyDialog,
            DropDownButton, TooltipDialog, _WidgetBase, _TemplatedMixin, _Container, template){


    return declare([_WidgetBase, _TemplatedMixin, _Container], {
        templateString: template,
        baseClass: "auth-manager",

        registrationDialogTitle: "Регистрация нового пользователя",
        verifyDialogTitle: "Проверка телефонного номера",
        loginDialogTitle: "Вход на сайт",

        registrationUrl: "/elastic/auth/registration/",
        verifyUrl: "/elastic/auth/verify/",
        reloadCodeUrl: "/elastic/auth/reload_code/",
        loginUrl: "/elastic/auth/login/",
        logoutURL: "/elastic/auth/logout/",

        _registrationDialog: null,
        _verifyDialog: null,
        _loginDialog: null,

        _updateControlBlock: function(){
            xhr.get('/elastic/auth/is_authenticated/', {
                handleAs: "json", // В ответ ждем данные в формате json
                headers: {"X-CSRFToken": cookie('csrftoken')}
            }).then(
                function(res){
                    domStyle.set('login-button', {'display': 'none'});
                    domStyle.set('registration-button', {'display': 'none'});
                    domStyle.set('logout-button', {'display': 'none'});
                    if(res.is_authenticated){ // Аутентифицирован
                        domStyle.set('logout-button', {'display': 'inline-block'});
                    }else{ // Не аутентифицирован
                        domStyle.set('login-button', {'display': 'inline-block'});
                        domStyle.set('registration-button', {'display': 'inline-block'});
                    }
                });
        },

        refresh: function(){
            this.inherited(arguments);
            this._updateControlBlock();
        },

        _registration: function(){ // Начать процедуру регистрации нового пользователя
            // Показываем диалог регистрации
            this._registrationDialog.show();
        },

        login: function(login, password){ // Начать процедуру аутентификации пользователя
            var self = this;
            var d = new Deferred();
            var df = new FormData();
            df.append("username", login);
            df.append("password", password);
            xhr.post(self.loginUrl, {
                handleAs: "json", // В ответ ждем данные в формате json
                headers: {"X-CSRFToken": cookie('csrftoken'), 'Content-Type': false},
                data: df
            }).then(
                function(res){
                    if (res.result == "NOT_VERIFY"){ // Показываем диалог проверки телефонного номера
                        self._login = login;
                        self._verifyDialog.show();
                    }else{
                        self._updateControlBlock();
                    }
                },
                function(err){ // Показываем сообщение об ошибке
                    self._registrationDialog.hide();
                    alert(err.response.text);
                }
            );
            return d;
        },

        logout: function(){
            var d = new Deferred();
            xhr.get(this.logoutURL, {
                headers: {"X-CSRFToken": cookie('csrftoken')}
            }).then(
                function(){ d.resolve(); },
                function(err){ // Показываем сообщение об ошибке
                    alert(err.response.text);
                }
            );
            return d;
        },

        init: function(){
            var self = this;

            // Вешаем обработчик на кнопку "Регистрация"
            on(dom.byId("registration-button"), "click", function(){
                self._registration();
                return false;
            });

            // Вешаем обработчик на кнопку "Выход"
            on(dom.byId("logout-button"), "click", function(){
                self.logout().then(function(){
                    self._updateControlBlock();
                });
                return false;
            });

            // Создаем всплывающий диалог входа
            var tooltipDialogLogin = new TooltipDialog({
                id: "tooltip-dialog-login",
                onMouseLeave: function(){
                    self._loginDialog.closeDropDown(false);
                },
                content: '' +
                    '<div class="row">' +
                        '<div>' +
                            '<label for="name">Логин (телефон, укзанный при регистрации):</label>' +
                        '</div>' +
                        '<input data-dojo-type="dijit/form/TextBox" id="tooltip-login" name="login">' +
                    '</div>' +
                    '<div class="row">' +
                        '<div>' +
                            '<label for="name">Пароль:</label>' +
                        '</div>' +
                        '<input type="password" data-dojo-type="dijit/form/TextBox" id="tooltip-password" name="password">' +
                    '</div>' +
                    '<button data-dojo-type="dijit/form/Button" type="submit">Войти</button>',
                onExecute: function(){
                    self.login(
                        registry.byId("tooltip-login").get("value"),
                        registry.byId("tooltip-password").get("value")
                    ).then(function(){
                        self._updateControlBlock();
                    });
                }
            });

            // Создаем кнопку "Вход"
            this._loginDialog = new DropDownButton({
                label: "Вход",
                dropDown: tooltipDialogLogin
            }, "login-button");


            // Вешаем обработчик на событие наведения мыши на кнопку "Вход"
            on(dom.byId('login-button'), 'mouseover', function(){
                self._loginDialog.openDropDown();
            });

            // Обновляем элементы управления
            this._updateControlBlock();


            // Создаем диалог регистрации
            this._registrationDialog = new RegistrationDialog(
                {
                    refocus: false,
                    title: this.registrationDialogTitle
                },
                this.registrationDialogNode
            );


            // Создаем диалог проверки кода
            this._verifyDialog = new VerifyDialog(
                {
                    refocus: false,
                    title: this.verifyDialogTitle
                },
                this.verifyDialogNode
            );

            // Ставим обработчик на событие клика на кнопке "Подать сведения"
            this._registrationDialog.get('submitButton').set('onClick', function(){
                self._login = self._registrationDialog.get('customerPhone').get('value');

                // Формируем данные для отправки на сервер
                var df = new FormData();
                df.append('first_name', self._registrationDialog.get('customerFirstName').get('value'));
                if (self._registrationDialog.get('customerMiddleName').get('value')){
                    df.append('middle_name', self._registrationDialog.get('customerMiddleName').get('value'));
                }
                df.append('last_name', self._registrationDialog.get('customerLastName').get('value'));
                df.append(
                    'birthday',
                    stamp.toISOString(
                        new Date(self._registrationDialog.get('customerBdDate').get('value')),
                        {selector: 'date'}
                    )
                );
                df.append('phone', self._registrationDialog.get('customerPhone').get('value'));
                df.append('email', self._registrationDialog.get('customerEmail').get('value'));
                df.append('password', self._registrationDialog.get('customerPassword').get('value'));

                // Отправляем данные
                xhr.post(self.registrationUrl, {
                    handleAs: "json", // В ответ ждем данные в формате json
                    headers: {"X-CSRFToken": cookie('csrftoken'), 'Content-Type': false},
                    data: df
                }).then(
                    function(res){ // Показываем диалог проверки телефонного номера
                        self._registrationDialog.hide();
                        self._verifyDialog.show();
                    },
                    function(err){ // Показываем сообщение об ошибке
                        self._registrationDialog.hide();
                        alert(err.response.text);
                    }
                );
            });

            // Ставим обработчик на событие клика на кнопке "Подтвердить"
            this._verifyDialog.get('submitButton').set('onClick', function(){
                // Формируем данные для отправки на сервер
                var df = new FormData();
                df.append("verify_code", self._verifyDialog.get('verifyCode').get('value'));
                df.append("username", self._login);
                // Отправляем данные
                xhr.post(self.verifyUrl, {
                    handleAs: "json", // В ответ ждем данные в формате json
                    headers: {"X-CSRFToken": cookie('csrftoken'), 'Content-Type': false},
                    data: df
                }).then(
                    function(){
                        self._verifyDialog.hide();
                        self._updateControlBlock();
                    },
                    function(err){ // Показываем сообщение об ошибке
                        alert(err.response.text);
                    }
                );
            });

            // Ставим обработчик на событие клика на кнопке "Запросить код повторно"
            this._verifyDialog.get('reloadCodeButton').set('onClick', function(){
                // Формируем данные для отправки на сервер
                var df = new FormData();
                df.append("username", self._login);
                // Отправляем данные
                xhr.post(self.reloadCodeUrl, {
                    handleAs: "json", // В ответ ждем данные в формате json
                    headers: {"X-CSRFToken": cookie('csrftoken'), 'Content-Type': false},
                    data: df
                }).then(
                    function(){ },
                    function(err){ // Показываем сообщение об ошибке
                        alert(err.response.text);
                    }
                );
            });

        },

        postCreate: function(){
            this.inherited(arguments);
            this.containerNode.innerHTML = template;
            var self = this;

            self.init();
        },

        startup: function() {
        }
    });
});
