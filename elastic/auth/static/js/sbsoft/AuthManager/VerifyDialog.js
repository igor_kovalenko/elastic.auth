/**
 * Author: "SB-Soft Ltd"
 * Contact: "support@sb-soft.biz"
 * Company: "SB-Soft Ltd"
 * Year: 2014
 * Version: "0.0.1"
 * Description: ""
 */

define([
    "dojo/_base/declare",
    'dojo/dom',
    "dijit/Dialog",
    "dojo/parser",
    "dijit/form/ValidationTextBox",
    "dijit/form/DateTextBox",
    "dijit/form/Button"
], function(declare, dom, Dialog, parser, ValidationTextBox, DateTextBox, Button){


    return declare("sbsoft/elastic/auth/VerifyDialog", [Dialog], {
        baseClass: 'verify-dialog',

        verifyCode: null,
        reloadCodeButton: null,
        submitButton: null,

        startup: function(){
            this.verifyCode.startup();
            this.reloadCodeButton.startup();
            this.submitButton.startup();
        },

        init: function(){
            console.debug("RegistrationDialog::init: I'm started!");
            var self = this;

            this.verifyCode = new ValidationTextBox({ required: true }, dom.byId(this.baseClass + "-" +
                'verify-code-input'));

            this.reloadCodeButton = new Button({ label: "Запросить код повторно"},
                dom.byId(this.baseClass + "-" + 'reload-code-button'));

            this.submitButton = new Button({ onClick: function(){ self.hide(); }, label: "Подтвердить" },
                dom.byId(this.baseClass + "-" + 'submit-button'));

            this.startup();
        },

        hide: function(){
            this['inherited'](arguments);
        },

        postCreate: function(){
            var self = this;
            this['inherited'](arguments);
            this['containerNode']['innerHTML'] =
                '<div class="auth-manager verify-dialog dialog">' +
                    '<div class="content">' +
                        '<div class="row">' +
                            '<p>На указанный Вами при регистрации номер мобильного телефона</p>' +
                            '<p>выслано СМС-сообщение с проверочным кодом</p>' +
                            '<p>Пожалуйста, введите проверочный код и нажмите кнопку "Подтвердить".</p>' +
                            '<p>Если сообщение не приходит более 3-х минут, нажмите на кнопку</p>' +
                            '<p>"Запросить код повторно".</p>' +
                        '</div>' +
                        '<div class="row">' +
                            '<div><label for="' + self.baseClass + 'verify-code-input">Проверочный код</label></div>' +
                            '<input id="' + self.baseClass + '-verify-code-input">' +
                        '</div>' +
                    '</div>' +
                    '<div id="' + self.baseClass + '-reload-code-button"></div>' +
                    '<div id="' + self.baseClass + '-submit-button">Подтвердить</div>' +
                '</div>';
            parser.parse(this['containerNode'], {noStart: false}).then(function(){
                self.init();
            });
        }
    });
});
