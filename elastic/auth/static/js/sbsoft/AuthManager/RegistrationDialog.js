/**
 * Author: "SB-Soft Ltd"
 * Contact: "support@sb-soft.biz"
 * Company: "SB-Soft Ltd"
 * Year: 2014
 * Version: "0.0.1"
 * Description: ""
 */

define([
    "dojo/_base/declare",
    'dojo/dom',
    "dijit/Dialog",
    "dojo/parser",
    "dijit/form/ValidationTextBox",
    "dijit/form/DateTextBox",
    "dijit/form/Button"
], function(declare, dom, Dialog, parser, ValidationTextBox, DateTextBox, Button){


    return declare("sbsoft/elastic/auth/RegistrationDialog", [Dialog], {
//        templateString: template,
        baseClass: 'registration-dialog',

        customerFirstName: null,
        customerMiddleName: null,
        customerLastName: null,
        customerBdDate: null,
        customerPhone: null,
        customerEmail: null,
        customerPassword: null,
        submitButton: null,

        startup: function(){
            this.customerFirstName.startup();
            this.customerMiddleName.startup();
            this.customerLastName.startup();
            this.customerBdDate.startup();
            this.customerPhone.startup();
            this.customerEmail.startup();
            this.submitButton.startup();
        },

        init: function(){
            console.debug("RegistrationDialog::init: I'm started!");
            var self = this;

            this.customerFirstName = new ValidationTextBox({
                promptMessage: "Укажите Ваше имя", name: "first_name", required: true
            }, dom.byId(this.baseClass + "-" + 'customer-first-name'));

            this.customerMiddleName = new ValidationTextBox({
                promptMessage: "Укажите Ваше отчество", name: "middle_name", required: false
            }, dom.byId(this.baseClass + "-" + 'customer-middle-name'));

            this.customerLastName = new ValidationTextBox({
                promptMessage: "Укажите Вашу фамилию", name: "last_name", required: true
            }, dom.byId(this.baseClass + "-" + 'customer-last-name'));

            this.customerBdDate = new DateTextBox({name: "birthday"}, dom.byId(this.baseClass + "-" + 'customer-bd-date'));

            this.customerPhone = new ValidationTextBox({
                promptMessage: "Укажите номер Вашего телефона",
                invalidMessage: "Телефон должен быть в формате 8XXXXXXXXXX",
                regExp: "8\\d{10}",
                name: "phone", required: true
            }, dom.byId(this.baseClass + "-" + 'customer-phone'));

            this.customerEmail = new ValidationTextBox({
                promptMessage: "Укажите Ваш адрес эл. почты",
                invalidMessage: "Неверный адрес эл. почты",
                regExp: "[\\w]+@[\\w]+\\.[\\w]{2,3}",
                name: "email", required: true
            }, dom.byId(this.baseClass + "-" + 'customer-email'));

            this.customerPassword = new ValidationTextBox({
                promptMessage: "Укажите пароль",
                invalidMessage: "Пароль должен быть не менее 7 символов",
                regExp: ".{7,}",
                type: 'password',
                name: "last_name", required: true
            }, dom.byId(this.baseClass + "-" + 'customer-password'));

            this.submitButton = new Button({
                onClick: function(){
                    self.hide();
                },
                label: "Подать сведения"
            }, dom.byId(this.baseClass + "-" + 'submit-button'));

            this.startup();
        },

        hide: function(){
            this['inherited'](arguments);
        },

        postCreate: function(){
            var self = this;
            this['inherited'](arguments);
            this['containerNode']['innerHTML'] =
                '<div class="auth-manager registration-dialog dialog">' +
                    '<div class="content">' +
                        '<div class="row">' +
                            '<div><label for="' + self.baseClass + '-customer-first-name">Имя</label></div>' +
                            '<input id="' + self.baseClass + '-customer-first-name"/>' +
                        '</div>' +
                        '<div class="row">' +
                            '<div><label for="' + self.baseClass + '-customer-middle-name">Отчество</label></div>' +
                            '<input id="' + self.baseClass + '-customer-middle-name">' +
                        '</div>' +
                        '<div class="row">' +
                            '<div><label for="' + self.baseClass + '-customer-last-name">Фамилия</label></div>' +
                            '<input id="' + self.baseClass + '-customer-last-name">' +
                        '</div>' +
                        '<div class="row">' +
                            '<div><label for="' + self.baseClass + '-customer-bd-date">День рождения</label></div>' +
                            '<input id="' + self.baseClass + '-customer-bd-date"/>' +
                        '</div>' +
                        '<div class="row">' +
                            '<div><label for="' + self.baseClass + '-customer-phone">Телефон</label></div>' +
                            '<input id="' + self.baseClass + '-customer-phone">' +
                        '</div>' +
                        '<div class="row">' +
                            '<div><label for="' + self.baseClass + '-customer-email">Адрес эл. почты</label></div>' +
                            '<input id="' + self.baseClass + '-customer-email">' +
                        '</div>' +
                        '<div class="row">' +
                            '<div><label for="' + self.baseClass + '-customer-password">Пароль</label></div>' +
                            '<input id="' + self.baseClass + '-customer-password">' +
                        '</div>' +
                    '</div>' +
                    '<div id="' + self.baseClass + '-submit-button"></div>' +
                '</div>';

            parser.parse(this['containerNode'], {noStart: false}).then(function(){
                self.init();
            });
        }
    });
});
