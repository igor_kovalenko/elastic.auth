# -*- coding: utf-8 -*-

#
# Copyright (c) 2013 SB-Soft Ltd
#

__author__ = "SB-Soft Ltd"
__contact__ = "support@sb-soft.biz"
__company__ = "SB-Soft Ltd"
__year__ = "2013"
__version__ = "0.1"
__description__ = ""

from django.conf.urls import patterns, url
from views import *

urlpatterns = patterns(
    '',
    url(r'^registration/$', auth_registration),
    url(r'^verify/$', auth_verify),
    url(r'^reload_code/$', auth_reload_verify_code),
    url(r'^login/$', login),
    url(r'^logout/$', logout),
    url(r'^is_authenticated/$', is_authenticated)
)
