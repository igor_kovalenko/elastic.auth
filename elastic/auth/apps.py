# -*- coding: utf-8 -*-

#
# Copyright (c) 2013 SB-Soft Ltd
#

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

__author__ = "SB-Soft Ltd"
__contact__ = "support@sb-soft.biz"
__company__ = "SB-Soft Ltd"
__year__ = "2015"
__version__ = "0.1"
__description__ = "Elastic auth package"


class ElasticAuth(AppConfig):
    name = 'elastic.auth'
    label = "elastic_auth"
    verbose_name = _('Elastic auth')
