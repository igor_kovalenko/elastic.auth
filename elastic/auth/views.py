# -*- coding: utf-8 -*-

#
# Copyright (c) 2013 SB-Soft Ltd
#

from django.shortcuts import HttpResponse, render_to_response, redirect
from django.core.context_processors import csrf
from django.http.response import HttpResponseBadRequest
from elastic.party.models import Party, LinkChannel
from elastic.party.forms import PartyForm
from elastic.organization.models import Counterparty
from django.contrib.auth.models import User
from django.contrib import auth
import json
from forms import AuthenticationForm
from django.forms import ValidationError
import random
from django.conf import settings
import urllib2
import urllib
from django.db import transaction
from django.http import QueryDict
from django.utils.datastructures import MultiValueDictKeyError
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.contrib.sites.models import Site

__author__ = "SB-Soft Ltd"
__contact__ = "support@sb-soft.biz"
__company__ = "SB-Soft"
__year__ = "2013"
__version__ = "0.1"
__description__ = "Elastic auth package"


def build_random_code():
    random.seed()
    return random.randint(100000, 1000000)


def send_sms(phone, login, password, request):
    print verify_code
    print phone
    request = urllib2.Request(settings.SMSC['url'])
    request.get_method = lambda: 'POST'
    values = {
        'login': settings.SMSC['login'],
        'psw': settings.SMSC['password'],
        'sender': '{0}'.format(request.get_host()),
        'subj': 'Verification code',
        'phones': phone,
        'mes': "Verification code for website universad.ru: {0}".format(verify_code)
    }
    request.add_data(urllib.urlencode(values))
    print urllib2.urlopen(request).read()


@transaction.atomic
def auth_registration(request):
    """
    Добавляем нового сотрудника (флаг достоверности полученных сведений не выставлен)
     и отправляем СМС с проверочным кодом на указанный телефон
     Сам проверочный код храним в сессии
    :param request:
    :return:
    """
    if request.POST:
        # print request.POST
        params = request.POST.copy()
        phone = params.pop('phone')[0]

        try:
            LinkChannel.objects.get(value=phone)
        except ObjectDoesNotExist:
            username = phone
        except MultipleObjectsReturned:
            username = build_random_code()
        else:
            username = build_random_code()
        finally:
            password = build_random_code()

        party_form = PartyForm(params)

        if party_form.is_valid():
            party = Party.objects.create(name=username)
            party.link_channels.create(name="Mobile", value=phone)
            Counterparty.objects.create(
                party=party,
                user=User.objects.create_user(username=username, password=password)
            )
            request.session['login'] = username
            send_sms(request.session['login'], phone)
            return HttpResponse()  # 200 (OK)
        else:
            print party_form.errors
    return HttpResponseBadRequest


@transaction.atomic
def auth_verify(request):
    """
    Получаем в запросе проверочный код и сравниваем его с хранящимся в сессии.
    Если они совпадают, - считаем что сведения о телефоне доставерны
    :param request:
    :return:
    """
    if request.method == 'POST':
        if str(request.session['verify_code']) == str(request.POST['verify_code']):
            user = User.objects.get(username=request.POST['username'])
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            party = Counterparty.objects.get(user=user).party
            phone = party.link_channels.get(name="Mobile", value=request.POST['username'])
            phone.is_verified = True
            phone.save()
            auth.login(request, user)
            return HttpResponse()  # 200 (OK)
    return HttpResponseBadRequest("Вы ввели не правильный код!")


def auth_reload_verify_code(request):
    """
    Создаем новый проверочный код и, сохранив его в сессии, отправляем на указанный телефон
    :param request:
    :return:
    """
    if request.method == 'POST':
        print request.POST
        request.session['verify_code'] = build_random_code()
        send_sms(request.session['verify_code'], request.POST['username'])
        return HttpResponse()  # 200 (OK)
    return HttpResponseBadRequest()


def login(request):
    if request.method == 'GET':
        c = {'form': AuthenticationForm()}
        c.update(csrf(request))
        return render_to_response("elastic/auth/login.html", c)

    if request.method == 'POST':
        try:
            redirect_url = QueryDict(request.META.get('HTTP_REFERER').split('?')[-1])['next']
        except MultiValueDictKeyError:
            redirect_url = "/"
        except KeyError:
            redirect_url = "/"

        user = User.objects.get(username=request.POST['username'])
        if not user.is_superuser and not user.is_staff:  # Для персонала и админа тел. не проверяем
            party = Counterparty.objects.get(user=user).party
            phone = party.link_channels.filter(name="Mobile")[0]
            if not phone.is_verified:
                request.session['verify_code'] = build_random_code()
                send_sms(request.session['verify_code'], phone.value)
                return HttpResponse(json.dumps({"result": "NOT_VERIFY"}))

        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            try:
                form.clean()
                auth.login(request, form.get_user())
                return redirect(redirect_url)
            except ValidationError:
                return HttpResponseBadRequest()
    return HttpResponseBadRequest()


def logout(request):
    auth.logout(request)
    return redirect(request.META.get('HTTP_REFERER', "/elastic/auth/login/"))


def is_authenticated(request):
    return HttpResponse(json.dumps({"is_authenticated": request.user.is_authenticated()}))
