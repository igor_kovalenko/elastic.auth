# -*- coding: utf-8 -*-

#
# Copyright (c) 2013 SB-Soft Ltd
#

__author__ = "SB-Soft Ltd"
__contact__ = "support@sb-soft.biz"
__company__ = "SB-Soft"
__year__ = "2013"
__version__ = "0.1"
__description__ = "Elastic auth package"

# from django.db import models
